﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	[System.Serializable]		// enables unity to see class variables in editor window
	public class PlayerStats {
		public int Health = 100;
	}

	//initialize the playerStats
	public PlayerStats playerStats = new PlayerStats();

	public int fallBoundary = -20; 

	void Update(){
		if(transform.position.y <= fallBoundary) {
			DamagePlayer(999999999);
		}
	}

	public void DamagePlayer(int damage) {
		playerStats.Health -= damage;
		if(playerStats.Health <= 0) {
			GameMaster.KillPlayer (this);
		}
	}

}
