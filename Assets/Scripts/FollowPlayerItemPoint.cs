﻿using UnityEngine;
using System.Collections;

public class FollowPlayerItemPoint : MonoBehaviour
{

		private PlayerItemPoint itemPoint;
		private float nextTimeToSearch = 0;
		private Transform target;
		
		

		// Use this for initialization
		void Start ()
		{
				GameObject player = GameObject.Find ("Weapon Hardpoint");
				itemPoint = player.GetComponent<PlayerItemPoint> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (transform == null) {
						return;
				}

				if (itemPoint == null) {
					FindPlayer();
					return;
				}
				transform.position = itemPoint.transform.position;

				Vector3 currentRotation = transform.rotation.eulerAngles;
				if (itemPoint.isRight) {
						currentRotation.y = 90;
				} else {
						currentRotation.y = -90;
				}
				transform.rotation = Quaternion.Euler (currentRotation);
		}


		void FindPlayer() {
			if(nextTimeToSearch <= Time.time) {
				GameObject searchResult = GameObject.Find("Weapon Hardpoint");
				if(searchResult != null) {
				itemPoint = searchResult.GetComponent<PlayerItemPoint>();
					//itemPoint = searchResult.transform;
				}
				nextTimeToSearch = Time.time + 0.5f;
			}
		}
}
