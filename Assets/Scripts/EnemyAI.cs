﻿using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class EnemyAI : MonoBehaviour {

	// What to chase
	public Transform target;

	// How many times each second we will update our path
	public float updateRate = 2f;

	// The AI's speed per second
	public float speed = 300f;

	// Way to change between force and impulse, how force is applied to the rigid body, gives control to 
	// how enemy behaves
	public ForceMode2D fMode;

	// Max distance from the AI to a way point for it to continue to the next wayoint
	public float nextWaypointDistance = 3;

	public float xOffset = 0;
	public float yOffset = 0;
	public float followDistance = 20;


	// Caching
	private Seeker seeker;
	private Rigidbody2D rb;

	// Starting position - used for returning home
	private Vector3 homePos;

	// Targeting position - used for offset targeting
	private Vector3 targetPos;

	// The Calculated Path
	public Path path;

	[HideInInspector]
	public bool pathIsEnded = false;

	// The waypoint we are currently moving towards
	private int currentWaypoint = 0;

	// Variable for when to search for respawned player. 
	private float nextTimeToSearch = 0f;

	// Boolean to return, ignoring the player target
	private bool returnHome = false;


	void Start (){
		seeker = GetComponent<Seeker> ();
		rb = GetComponent<Rigidbody2D> ();
		homePos = transform.position;

		if(target == null) {
			//Debug.LogError("NO PLAYER FOUND");
			return;
		}

		// Offset the position of the target
		targetPos.x = target.position.x + xOffset;
		targetPos.y = target.position.y + yOffset;

		// Start a new path to the target position, return the result to the OnPathComplete method
		seeker.StartPath(transform.position, targetPos, OnPathComplete);
		//Debug.Log (":::UPDATING:::  Pos X: " + target.position.x + "   Pos Y: " + target.position.y);

		StartCoroutine (UpdatePath());
	}


	IEnumerator UpdatePath() {
		//Debug.Log (".....update path..........");
		yield return new WaitForSeconds (1f/updateRate);

		if(target == null) {
			//Debug.Log ("NO PLAYER / TARGET");
			ReturnHome ();
			FindPlayer();
			return false;
		}

		// Offset the position of the target
		targetPos.x = target.position.x + xOffset;
		targetPos.y = target.position.y + yOffset;

		float dist = Vector3.Distance (homePos, transform.position);
		if(dist >= followDistance) {
			returnHome = true;
		// may need to adjust how close to home is necessary, never reaches exact 0
		} else if(dist < 2){
			returnHome = false;
		}


		if(!returnHome){
			// Start a new path to the target position, return the result to the OnPathComplete method
			seeker.StartPath(transform.position, targetPos, OnPathComplete);
			//Debug.Log (":::UPDATING:::  Pos X: " + target.position.x + "   Pos Y: " + target.position.y);
		} else {
			// Start a new path to the target position, return the result to the OnPathComplete method
			seeker.StartPath(transform.position, homePos, OnPathComplete);
			//Debug.Log (":::UPDATING:::  Pos X: " + target.position.x + "   Pos Y: " + target.position.y);
		}

		StartCoroutine (UpdatePath ());
	}

	
	public void OnPathComplete(Path p) {
		//Debug.Log ("Path Found - Error? " + p.error);
		if(!p.error) {
			path = p;
			currentWaypoint = 0;
		}
	}

	
	// Use for moving things based on physics calculations
	void FixedUpdate(){
		//TODO: Always look at player

		if(path == null) {
			//Debug.Log ("PATH = NULL");
			return;
		}

		if(currentWaypoint >= path.vectorPath.Count) {
			if(pathIsEnded) {
				//Debug.Log ("PATHISENDED TRUE");
				return;
			}

			//Debug.Log ("End of path reached.");
			pathIsEnded = true;
			return;
		}

		pathIsEnded = false;

		// Direction to next waypoint
		Vector3 dir = (path.vectorPath [currentWaypoint] - transform.position).normalized;
		dir  *= speed * Time.fixedDeltaTime;

		// Move the AI
		rb.AddForce (dir, fMode);

		float dist = Vector3.Distance (transform.position, path.vectorPath [currentWaypoint]);
		if(dist < nextWaypointDistance) {
			currentWaypoint++;
			//Debug.Log ("CWP: " + currentWaypoint + "   Dist: " + dist);
			return;
		}

	}


	// Search for the respawned player
	void FindPlayer() 
	{
		if(nextTimeToSearch <= Time.time) {
			GameObject searchResult = GameObject.FindGameObjectWithTag("Player");
			if(searchResult != null) {
				target = searchResult.transform;
				//Debug.Log ("TARGET FOUND - x: " + target.position.x + "  y: " + target.position.y); 
				StartCoroutine (UpdatePath());
			}
			nextTimeToSearch = Time.time + 0.5f;
		}
	}

	void ReturnHome() {
		seeker.StartPath(transform.position, homePos, OnPathComplete);
		//Debug.Log (":::HOME UPDATING:::  Pos X: " + homePos.x + "   Pos Y: " + homePos.y);
		StartCoroutine (UpdatePath());
	}

}




























