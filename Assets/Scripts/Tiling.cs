﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class Tiling : MonoBehaviour
{
	public int OffsetX = 20;

	//Flips sprites that aren't tilable in order to make them tilable
	public bool ReverseScale = false;
	
	private float SpriteWidth = 0f;
	
	private Camera _camera;
	private Transform _transform;
	private Transform _rightBuddy;
	private Transform _leftBuddy;
	
	void Awake()
	{
		_camera = Camera.main;
		_transform = transform;
	}
	
	void Start ()
	{
		// assign the sprite renderer and check the width
		var sRenderer = GetComponent<SpriteRenderer>();
		SpriteWidth = sRenderer.sprite.bounds.size.x;

		// get position of the rightBuddy tile
		var newPositionA = new Vector3(_transform.position.x + SpriteWidth*2, _transform.position.y, _transform.position.z);

		// instantiate the new buddy sprite element with the given position
		_rightBuddy = Instantiate(_transform, newPositionA, _transform.rotation) as Transform;

		// group rightBuddy under the same parent
		_rightBuddy.parent = _transform.parent;

		Destroy(_rightBuddy.GetComponent<Tiling>());

		if (ReverseScale)
			HorizontalFlip(_rightBuddy);
		
		// get position of the leftBuddy tile
		var newPositionB = new Vector3(_transform.position.x - SpriteWidth*2, _transform.position.y, _transform.position.z);

		// instantiate the new buddy sprite element with the given position
		_leftBuddy = Instantiate(_transform, newPositionB, _transform.rotation) as Transform;

		// group leftBuddy under the same parent
		_leftBuddy.parent = _transform.parent;

		Destroy(_leftBuddy.GetComponent<Tiling>());

		if (ReverseScale)
			HorizontalFlip(_leftBuddy);
	}
	

	// Update is called once per frame
	void Update () {
		// calculate length from center of camera to the right edge (half of right) of what the camera can see in world coordinates
		float camHorizontalExtend = _camera.orthographicSize*Screen.width/Screen.height;

		// calculate the x position where the camera can see the edge of the sprite (element)
		float edgeVisiblePositionRight = (_transform.position.x + SpriteWidth + SpriteWidth / 2) - camHorizontalExtend;
		float edgeVisiblePositionLeft = (_transform.position.x - SpriteWidth - SpriteWidth / 2) + camHorizontalExtend;
		
		// checking if we can see the right edge of the element and then shifting left buddy into position accordingly
		if (_camera.transform.position.x + OffsetX >= edgeVisiblePositionRight)
		{
			var newPosition = new Vector3(_rightBuddy.position.x + SpriteWidth*2, _rightBuddy.position.y, _rightBuddy.position.z);
			_leftBuddy.position = newPosition;
			if (ReverseScale)
				HorizontalFlip(_leftBuddy);
			
			var temp = _transform;
			_transform = _rightBuddy;
			_rightBuddy = _leftBuddy;
			_leftBuddy = temp;
		}
		// checking if we can see the left edge of the element and then shifting right buddy into position accordingly
		else if (_camera.transform.position.x - OffsetX <= edgeVisiblePositionLeft)
		{
			var newPosition = new Vector3(_leftBuddy.position.x - SpriteWidth*2, _leftBuddy.position.y,
			                              _leftBuddy.position.z);
			_rightBuddy.position = newPosition;
			if (ReverseScale) 
				HorizontalFlip(_rightBuddy);
			
			var temp = _transform;
			_transform = _leftBuddy;
			_leftBuddy = _rightBuddy;
			_rightBuddy = temp;
		}
	}

	// Used for making non tilable sprites tilable by flipping them
	void HorizontalFlip(Transform tranform)
	{
		tranform.localScale = new Vector3(tranform.localScale.x * -1, tranform.localScale.y, tranform.localScale.z);
	}
	
}

























