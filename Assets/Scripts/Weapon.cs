﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public Transform target;
	public float fireRate = 2;		
	public float Damage = 10;
	public LayerMask whatToHit;
	public Transform BulletTrailPrefab;
	public Transform MuzzleFlashPrefab;
	public float effectSpawnRate = 10;

	float timeToSpawnEffect = 0;


	float timeToFire = 0;
	Transform firePoint;

	// Use this for initialization
	void Awake () {
		firePoint = transform.FindChild ("FirePoint");
		if(firePoint == null) {
			Debug.LogError("No child FirePoint");
		}
	}


	// Update is called once per frame
	void Update () {
		if(fireRate == 0) {
			audio.Play ();
			Shoot();
		} else {
			if(Time.time > timeToFire) {
				timeToFire = Time.time + 1/fireRate;
				audio.Play ();
				Shoot();
			}
		}
	}


	void Shoot() {
		Vector2 targetPos = new Vector2 (target.position.x, target.position.y);

		Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);
		RaycastHit2D hit = Physics2D.Raycast (firePointPosition, targetPos-firePointPosition, 100, whatToHit);
		//RaycastHit2D hit = Physics2D.Raycast (targetPos-firePointPosition, firePointPosition, 100, whatToHit);

		// controls how quickly to draw projectiles
		if(Time.time >= timeToSpawnEffect) {
			Effect ();
			timeToSpawnEffect = Time.time + 1/effectSpawnRate;
		}
		
		//Debug.DrawLine (firePointPosition, (mousePosition-firePointPosition)*100, Color.cyan);
		if(hit.collider != null) {
			Debug.DrawLine (firePointPosition, hit.point, Color.red);
			Debug.Log ("Hit: " + hit.collider.name + "  Damage: " + Damage);
		}
	}


	void Effect() {
		// create objects
		Instantiate (BulletTrailPrefab, firePoint.position, firePoint.rotation);
		Transform clone = Instantiate (MuzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
		clone.parent = firePoint;
		float size = Random.Range (0.6f, 0.9f);
		clone.localScale = new Vector3 (size, size, size);
		Destroy (clone.gameObject, 0.02f);
	}




}
























